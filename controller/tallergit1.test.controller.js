'use strict';
const awaitErrorHandler = require('../util/error-handler');
const util = require('../util/util');
const constants = require('../config/constants');

const userPasswordBuffkey= require('../keys/userPassword.json')
const USERPASSWORD=Buffer.from(userPasswordBuffkey)


var test1 = awaitErrorHandler(async (req, res, next) => {
    var {variable} = req.body
    if (variable) {
        var usuario
        var resultado={
            variable:variable,
            passUser:USERPASSWORD.toString()
        }
        res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.EXITO, true, resultado));
    } else {
        res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.ERROR, false, "error"));
    }

});
exports.test1 = test1;    


var autentificacion = awaitErrorHandler(async (req,res,next)=>{
    var {usuario, password } = req.body
    try{
        if(usuario && password){
            if(usuario=='usuario1' && password==USERPASSWORD.toString()){
                res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.EXITO, true, "AUTH:TRUE")); 
            }else{
                res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.ERROR, false, "Error de credenciales"));     
            }
        }else{
            res.status(constants.CODIGO.SERVIDOR.MISSING);
            res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.MISSING, false, "Revisar parametros de ingreso"));  
        }
    }catch(err){
        console.log(err)
        res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.ERROR, false, "Error no esperado"));  
    }

})
exports.autentificacion=autentificacion;
