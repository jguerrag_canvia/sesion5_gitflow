'use strict';

module.exports = {
  CODIGO: {
    SERVIDOR: {
      EXITO: 200,
      ERROR: 500,
      ERROR_SIN_AUTORIZACION: 401,
      ERROR_APP: 400,
      MISSING: 404,
    }
  }
}