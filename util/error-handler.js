'use strict';
const constants = require('../config/constants');
const util = require('./util');

//middleware to hanlde errors 
var awaitErrorHandler = middleware => {
  return async (req, res, next) => {
    try {
      await middleware(req, res, next);
    } catch (err) {
      
      util.consoleLog(err);
      res.status(constants.CODIGO.SERVIDOR.ERROR);
      res.json(util.jsonResponse(constants.CODIGO.SERVIDOR.ERROR, constants.MENSAJE.SERVIDOR.ERROR, null));
    }
  };
};

module.exports = awaitErrorHandler;