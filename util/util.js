'use strict';
const config = require('../config/config');
var consoleLog = function (mensaje, err = null) {
  if (!config.production)
    console.log(mensaje, err);
};
exports.consoleLog = consoleLog;

var jsonResponse = function (code, message, data) {
  return {
    code: code,
    mensaje: message,
    data: data
  }
};
exports.jsonResponse = jsonResponse;